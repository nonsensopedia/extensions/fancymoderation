/*
 * Parts of this were originally written on Nonsensopedia by:
 * Expert3222, Ostrzyciel, Polskacafe
 * Licensed there under CC-BY-SA-3.0
 *
 * Module handling all revert-related stuff.
*/
function runRevertWithReason() {

	// RwrDialog code
	function RwrDialog( config ) {
		RwrDialog.super.call( this, config );
	}

	OO.inheritClass( RwrDialog, OO.ui.ProcessDialog );

	RwrDialog.static.name = 'rwrDialog';
	RwrDialog.static.size = 'medium';
	RwrDialog.static.title = mw.msg( 'fmod-rollback-reason-window-title' );

	RwrDialog.static.actions = [
		{
			action: 'continue',
			flags: ['primary', 'progressive'],
			label: mw.msg( 'fmod-rollback-reason-confirm' )
		},
		{
			flags: 'safe',
			label: mw.msg( 'cancel' )
		}
	];

	RwrDialog.prototype.initialize = function () {
		RwrDialog.super.prototype.initialize.call( this );

		this.panel = new OO.ui.PanelLayout( {
			padded: true,
			expanded: false
		} );

		var options = [];
		var reasons = mw.config.get( 'wgFmodRollbackReasons', [] );
		for ( i in reasons ) {
			options.push( new OO.ui.MenuOptionWidget( {
				data: i,
				label: reasons[i]
			} ) );
		}
		options.push( new OO.ui.MenuOptionWidget( {
			data: 'custom',
			label: mw.msg( 'fmod-rollback-reason-custom' )
		} ) );

		this.customReasonInput = new OO.ui.TextInputWidget( {
			placeholder: mw.msg( 'fmod-rollback-reason-custom-placeholder' )
		} );

		var dialog = this;
		this.customReasonInput.on( 'enter', function () {
			dialog.executeAction( 'continue' );
		} );
		this.customReasonField = new OO.ui.FieldLayout( this.customReasonInput, {
			label: mw.msg( 'fmod-rollback-reason-custom-label' ),
			align: 'top'
		} );
		this.customReasonField.toggle( false );

		this.dropdown = new OO.ui.DropdownWidget( {
			$overlay: this.$overlay,
			menu: {
				items: options
			}
		} );
		this.dropdown.getMenu().selectItemByData( '0' );

		var dropdown = this.dropdown;
		var customReasonField = this.customReasonField;
		this.dropdown.on( 'labelChange', function ( _ ) {
			if ( dropdown.getMenu().findSelectedItem().getData() === "custom" ) {
				customReasonField.toggle( true );
				dialog.setSize( 'medium' );
			} else {
				customReasonField.toggle( false );
				dialog.setSize( 'medium' );
			}
		} );

		this.content = new OO.ui.FieldsetLayout();
		this.field = new OO.ui.FieldLayout( this.dropdown, {
			align: 'top'
		} );

		this.content.addItems( [this.field, this.customReasonField] );
		this.panel.$element.append( this.content.$element );
		this.$body.append( this.panel.$element );
	};

	RwrDialog.prototype.getActionProcess = function ( action ) {
		if ( action === 'continue' ) {
			var dialog = this;
			return new OO.ui.Process( function () {
				var reason;
				if ( dialog.dropdown.getMenu().findSelectedItem().getData() === 'custom' ) {
					reason = dialog.customReasonInput.getValue();
				} else {
					reason = dialog.dropdown.getMenu().findSelectedItem().getLabel();
				}

				mw.messages.set(
					'fmod-rollback-reason-message',
					mw.config.get( 'wgFmodRollbackSummary' )
				);
				reason = mw.message(
					'fmod-rollback-reason-message',
					dialog.rwrData.user,
					reason
				).plain();
				revertWithReason(
					dialog.rwrData.pageTitle,
					dialog.rwrData.user,
					reason
				);

				dialog.close();
			} );
		}

		return RwrDialog.super.prototype.getActionProcess.call( this, action );
	};

	RwrDialog.prototype.getSetupProcess = function ( data ) {
		return RwrDialog.super.prototype.getSetupProcess.call( this, data )
			.next( function () {
				this.rwrData = data;
				this.field.setLabel(
					mw.msg( 'fmod-rollback-reason-list-label', data.pageTitle, data.user )
				);
			}, this );
	};

	//setup the WindowManager and other stuff
	var windowManager = new OO.ui.WindowManager();
	$( document.body ).append( windowManager.$element );
	var rwrDialog = new RwrDialog();
	windowManager.addWindows( [rwrDialog] );

	/**
	 * Helper for opening the revert with reason dialog.
	 */
	function showRevertWithReasonDialog() {
		var link = $( this );
		windowManager.openWindow( rwrDialog, {
			'pageTitle': link.attr( 'data-page-title' ),
			'user': link.attr( 'data-user' ),
		} );
	}

	/**
	 * Perform the actual revert.
	 *
	 * @param pageTitle
	 * @param user
	 * @param reason
	 */
	function revertWithReason( pageTitle, user, reason ) {
		var api = new mw.Api();
		api.postWithToken( 'rollback', {
			'action': 'rollback',
			'title': pageTitle,
			'user': user,
			'summary': reason,
		} ).done( function ( data ) {
			console.debug( data );
			var specialPageName = mw.config.get( 'wgCanonicalSpecialPageName' );

			if ( mw.util.getParamValue( 'diff' ) !== null || specialPageName === 'MobileDiff' ) {
				// if diff > go to top edit
				var title = new mw.Title( pageTitle );
				window.location = title.getUrl( { 'diff': data.rollback.revid } );
			} else if ( specialPageName === 'Recentchanges' ) {
				// if RC > message
				mw.notify(
					mw.msg( 'fmod-rollback-success', pageTitle, user ),
					{ 'type': 'success' }
				);
			} else {
				// otherwise (history page?) > reload
				window.location.reload();
			}

		} ).fail( function ( code ) {
			console.error( code );
			var message;
			if ( code === 'alreadyrolled' ) {
				message = mw.msg( 'fmod-alreadyrolled' );
			} else if ( code === 'onlyauthor' ) {
				message = mw.msg( 'fmod-onlyauthor' );
			} else {
				message = mw.msg( 'fmod-unknown-error', code );
			}
			mw.notify( message, {
				'autoHideSeconds': 'long',
				'type': 'error'
			} );
		} );
	}

	// Initialize links
	mw.hook( 'wikipage.content' ).add( function () {
		$( '.mw-rollback-reason-link > a' ).click( showRevertWithReasonDialog );
	} );
}

runRevertWithReason();
