/**
 * Common patrolling module for FancyModeration.
 * Authors: Eksekk, Ostrzyciel
 **/
mw.FmodPatrol = function () {
	var self = {};

	/**
	 * Patrol a single edit.
	 * Usage: mw.FmodPatrol.patrolChange( diff ).done( callback ).fail( callback2 );
	 *
	 * @param diff
	 */
	self.patrolChange = function ( diff ) {
		if ( diff === null || diff === "" || typeof diff == "undefined" ) {
			return $.Deferred().reject();
		}

		var api = new mw.Api();
		return api.postWithToken( 'patrol', {
			action: 'fmod-patrol',
			format: 'json',
			revid: diff
		} );
	}

	/**
	 * Patrol a range of consecutive edits on a page.
	 * Usage: mw.FmodPatrol.patrolRangeOfChanges( to, from ).done( callback ).fail( callback2 );
	 *
	 * @param to
	 * @param from
	 */
	self.patrolRangeOfChanges = function ( to, from ) {
		if ( to === null || to === "" || typeof to == "undefined" ) {
			return $.Deferred().reject();
		}

		var request = {
			action: 'fmod-patrol',
			format: 'json',
			maxrevid: to
		};
		if ( from !== null && from <= to && from >= 0 ) {
			$.extend( request, {
				minrevid: from
			} );
		}

		var api = new mw.Api();
		return api.postWithToken( 'patrol', request );
	};

	/**
	 * Patrol all edits on a page.
	 * Usage: self.patrolPage( pageid ).done( callback ).fail( callback2 );
	 *
	 * @param id
	 * @param tokenAlreadyReset
	 */
	self.patrolPage = function ( id, tokenAlreadyReset ) {
		if ( id === null || id === "" || typeof id == "undefined" ) {
			return $.Deferred().reject();
		}

		var api = new mw.Api();
		return api.postWithToken( 'patrol', {
			action: 'fmod-patrol',
			format: 'json',
			pageid: id
		} );
	};

	return self;
}();