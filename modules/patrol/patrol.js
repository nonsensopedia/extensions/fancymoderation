/*
 * Patrol links handling code
 * Authors: Eksekk, Ostrzyciel
 */

var imagePath = mw.config.get( 'wgExtensionAssetsPath' ) + '/FancyModeration/modules/patrol/';

var patrolSuccessCode = "<span style='color: green;' class='patrolButtonClicked'><img src='" +
	imagePath + "done.svg" + "' width='15px' height='15px' />&nbsp;" +
	mw.msg('fmod-patrol-done') + "</span>";
var patrolFailureCode = "<span style='color: red;' class='patrolButtonClicked'><img src='" +
	imagePath + "failed.svg" + "' width='15px' height='15px' />&nbsp;" +
	mw.msg('fmod-patrol-failed') + "</span>";

function bindPatrolLinks() {
	var specialPageName = mw.config.get( "wgCanonicalSpecialPageName" );

	if ( specialPageName === "Recentchanges" ) {
		patrolUI.bindRcPatrolLinks();
	} else if ( specialPageName === "Newpages" ) {
		var $buttons = $( ".patrollink-page a" );
		$buttons.on( 'click', handlerNoConcurrence( function () {
			var $button = $( this );
			processPagePatrol( $button.parent() );
		} ) );
	} else if ( mw.util.getParamValue( "diff" ) !== null ) {
		patrolUI.bindDiffPatrolLinks();
	} else if ( $( ".patrollink" ).length > 0 ) {
		// patrol link in the footer
		// remove default click handler and attach our own
		var $button = $( ".patrollink-page a" );
		$button.off( "click" ).on( 'click', handlerNoConcurrence( function ( event ) {
			event.preventDefault();
			event.stopImmediatePropagation();
			processPagePatrol( $button.parent() );
		} ) );
	} else if ( specialPageName === "MobileDiff" ) {
		patrolUI.bindDiffPatrolLinks();
	}
}

var patrolUI = ( function () {
	var init = {};

	/**
	 * Binds patrol links to events on RecentChanges.
	 */
	init.bindRcPatrolLinks = function () {
		$( ".patrollink-range a" ).each( function () {
			var $link = $( this );
			if ( !isEventBound( $link ) ) {
				var $tbody = getParentTag( $link, "tbody" );
				$link.on( 'click', handlerNoConcurrence( function () {
					processMultipleChangeRCPatrol( $tbody );
				} ) );
			}
		} );

		$( ".patrollink-single a" ).each( function () {
			var $button = $( this );
			if ( !isEventBound( $button ) ) {
				$button.on( 'click', handlerNoConcurrence( function () {
					processRCChangePatrol( $button.parent(), true );
				} ) );
			}
		} );
	};

	/**
	 * Binds patrol links to events in diff view.
	 */
	init.bindDiffPatrolLinks = function () {
		var $button = $( ".patrollink-range a" );
		if ( !isEventBound( $button ) ) {
			$button.on( 'click', handlerNoConcurrence( function () {
				processDiffPageRangePatrol();
			} ) );
		}

		$button = $( ".patrollink-page a" );
		if ( !isEventBound( $button ) ) {
			$button.on( 'click', handlerNoConcurrence( function () {
				processPagePatrol( $button.parent() );
			} ) );
		}
	}

	/**
	 * Changes patrol link's appearance to reflect that the patrol action was started.
	 *
	 * @param $patrolLink
	 * @param $patrolAgainBrackets
	 */
	init.startPatrol = function ( $patrolLink, $patrolAgainBrackets ) {
		var $spinner = $.createSpinner( {size: 'small', type: 'inline'} );
		$patrolLink.hide();
		$patrolAgainBrackets.hide();
		$patrolLink.after( $spinner );
		$patrolLink.parent().find( ".patrolButtonClicked" ).remove();
	};

	function removeSpinner( $node ) {
		$node.find( "div.mw-spinner" ).remove();
	}

	/**
	 * Makes the patrol link display a happy face and say "all is fine and dandy"
	 *
	 * @param data
	 * @param msg
	 * @param $button
	 * @param $patrolLink
	 * @param $patrolAgainBrackets
	 */
	init.showSuccess = function ( data, msg, $button, $patrolLink, $patrolAgainBrackets ) {
		removeSpinner( $button );

		$patrolLink.after( patrolSuccessCode );
		$patrolLink.remove();
		$patrolAgainBrackets.remove();

		var title = new mw.Title( data["fmod-patrol"].title );
		mw.notify( mw.msg( msg, title.toText() ) );
	};

	/**
	 * Makes the patrol link display an error.
	 *
	 * @param msg
	 * @param $button
	 * @param $patrolLink
	 * @param $patrolAgainBrackets
	 */
	init.showFailure = function ( msg, $button, $patrolLink, $patrolAgainBrackets ) {
		removeSpinner( $button );

		if ( $patrolAgainBrackets.length === 0 ) {
			$patrolLink.before( patrolFailureCode );
			$patrolLink.before( "<span class='patrol-again-bracket patrol-again-open'> (</span>" );
			$patrolLink.after( "<span class='patrol-again-bracket'>)</span>" );
		} else {
			$patrolAgainBrackets
				.filter( '.patrol-again-open' )
				.before( patrolFailureCode );
			$patrolAgainBrackets.show();
		}
		$patrolLink.text( mw.msg( 'fmod-patrol-try-again' ) );
		$patrolLink.show();

		mw.notify( mw.msg( msg ), {type: 'error'} );
	}

	return init;
} )();

/**
 * Transforms an event handler into one that won't run concurrently with itself.
 *
 * @param handler
 * @returns {function(...[*]=)}
 */
function handlerNoConcurrence( handler ) {
	var running = false;

	return function ( arg ) {
		if ( running ) {
			return;
		}
		running = true;
		handler( arg );
		running = false;
	}
}

/**
 * Process edit range patrol links on RC
 *
 * @param $tbody
 */
function processMultipleChangeRCPatrol( $tbody ) {
	var $button = $tbody.find( ".patrollink-range" ),
		$patrolLink = $button.find( "a" ),
		$singlePatrolLinks = $tbody.find( ".patrollink-single a" ),
		$patrolAgainBrackets = $patrolLink.parent().find( ".patrol-again-bracket" );

	patrolUI.startPatrol( $patrolLink, $patrolAgainBrackets );

	mw.FmodPatrol.patrolRangeOfChanges( $patrolLink.attr( "data-revision-max" ) ).done( function ( data ) {
		patrolUI.showSuccess(
			data,
			'markedaspatrollednotifymulti',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);

		//replace single buttons with success text
		$singlePatrolLinks.replaceWith( patrolSuccessCode );

		//remove patrol exclamation marks
		$tbody.find( "abbr.unpatrolled" ).replaceWith( "&nbsp;" );
	} ).fail( function() {
		patrolUI.showFailure(
			'markedaspatrollederrornotifymulti',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
	} );
}

/**
 * Process edit range patrol links
 */
function processDiffPageRangePatrol() {
	var $button = $( ".patrollink-range" ),
		$patrolLink = $button.find( "a" ),
		$patrolAgainBrackets = $button.find( ".patrol-again-bracket" ),
		to = $patrolLink.attr( "data-revision-max" ),
		from = $patrolLink.attr( "data-revision-min" );

	patrolUI.startPatrol( $patrolLink, $patrolAgainBrackets );

	mw.FmodPatrol.patrolRangeOfChanges( to, from ).done( function ( data ) {
		patrolUI.showSuccess(
			data,
			'markedaspatrollednotifymulti',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
	} ).fail( function () {
		patrolUI.showFailure(
			'markedaspatrollederrornotifymulti',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
	} );
}

/**
 * Process RC single change patrol link
 *
 * @param $button
 */
function processRCChangePatrol( $button ) {
	var $patrolLink = $button.find( "a" ),
		revid = $patrolLink.attr( "data-revision" ),
		$patrolAgainBrackets = $button.find( ".patrol-again-bracket" );

	patrolUI.startPatrol( $patrolLink, $patrolAgainBrackets );

	mw.FmodPatrol.patrolChange( revid ).done( function ( data ) {
		patrolUI.showSuccess(
			data,
			'markedaspatrollednotify',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
		getParentTag($button, "tr").find("abbr.unpatrolled").replaceWith("&nbsp;");
	} ).fail( function () {
		patrolUI.showFailure(
			'markedaspatrollederrornotifychange',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
	} );
}

/**
 * Process page patrol link
 *
 * @param $button
 */
function processPagePatrol( $button ) {
	// diff page, new pages and any new page on the bottom
	var pageid = $button.find( "a" ).attr( "data-pageid" ),
		$patrolLink = $button.find( "a" ),
		$patrolAgainBrackets = $button.find( ".patrol-again-bracket" );

	patrolUI.startPatrol( $patrolLink, $patrolAgainBrackets );

	mw.FmodPatrol.patrolPage( pageid ).done( function ( data ) {
		patrolUI.showSuccess(
			data,
			'markedaspatrollednotifypage',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);

		// if we are on diff page remove range patrol button, because we just patrolled all page revisions
		$( ".patrollink-range a" ).replaceWith( patrolSuccessCode );

		// if we are on Special:Newpages change background to match patrolled
		if ( mw.config.get( "wgCanonicalSpecialPageName" ) === "Newpages" ) {
			getParentTag( $button, "li" ).removeClass( "not-patrolled" );
		}
	} ).fail( function () {
		patrolUI.showFailure(
			'markedaspatrollederrornotifypage',
			$button,
			$patrolLink,
			$patrolAgainBrackets
		);
	} );
}

function getParentTag( $anchor, tag ) {
	tag = tag.toUpperCase();
	var $a = $anchor;
	while ( $a.prop( "tagName" ) !== tag && $a.prop( "tagName" ) !== "BODY" ) {
		$a = $a.parent();
	}
	return $a;
}

function isEventBound( $element ) {
	if ( $element == null || !$element.length ) {
		return false;
	}
	var events = $._data( $element.get( 0 ), "events" );
	return events != null;
}

// Bind to wikipage.content hook. This will cause the function to run when using
// RevisionSlider or EnhancedChangesList. Jazzy.
mw.hook( 'wikipage.content' ).add( bindPatrolLinks );

