<?php

use FancyModeration\Preferences;
use MediaWiki\MediaWikiServices;

return [
	'FmodPreferencesService' => function ( MediaWikiServices $services ) : Preferences {
		return new Preferences(
			$services->getUserOptionsLookup(),
			$services->getPermissionManager()
		);
	},
];
