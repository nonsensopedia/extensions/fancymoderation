<?php

namespace FancyModeration;

use Html;
use IContextSource;

class PatrolLinker {

	/**
	 * Make a link for patrolling a range of edits.
	 *
	 * @param IContextSource $context
	 * @param $maxRev
	 * @param int $minRev
	 *
	 * @return string
	 */
	public static function makePatrolRangeLink( IContextSource $context, $maxRev, $minRev = 0 ) : string {
		$attrs = [
			'title' => $context->msg( 'fmod-patrol-range-tooltip' )->text(),
			'data-revision-max' => $maxRev
		];

		if ( $minRev ) {
			$attrs['data-revision-min'] = $minRev;
		}

		return Html::element(
			'a',
			$attrs,
			$context->msg( 'fmod-patrol-range' )->text()
		);
	}

	/**
	 * Make a link for patrolling a single edit.
	 *
	 * @param IContextSource $context
	 * @param int $revId
	 *
	 * @return string
	 */
	public static function makePatrolSingleLink( IContextSource $context, int $revId ) : string {
		return Html::element(
			'a',
			[
				'title' => $context->msg( 'fmod-patrol-tooltip' )->text(),
				'data-revision' => $revId
			],
			$context->msg( 'fmod-patrol' )->text()
		);
	}

	/**
	 * Make a link for patrolling all changes on a page.
	 *
	 * @param IContextSource $context
	 * @param $pageId
	 *
	 * @return string
	 */
	public static function makePatrolPageLink( IContextSource $context, $pageId ) : string {
		return Html::element(
			'a',
			[
				'title' => $context->msg( 'fmod-patrol-page-tooltip' )->text(),
				'data-pageid' => $pageId
			],
			$context->msg( 'fmod-patrol-page' )->text()
		);
	}
}