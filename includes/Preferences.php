<?php

namespace FancyModeration;

use MediaWiki\Permissions\PermissionManager;
use MediaWiki\User\UserOptionsLookup;
use User;

class Preferences {
	public const UP_USE_ROLLBACK_WITH_REASON = 'userollbackwithreason';
	public const UP_USE_ROLLBACK_CONFIRMATION = 'userollbackconfirmation';
	public const UP_USE_QUICK_PATROL_LISTS = 'usequickpatrollists';
	public const UP_USE_QUICK_PATROL_NEWPAGES = 'usequickpatrolnewpages';
	public const UP_USE_QUICK_PATROL_DIFFS = 'usequickpatroldiffs';
	public const UP_USE_QUICK_PATROL_FOOTER = 'usequickpatrolfooter';

	/** @var UserOptionsLookup */
	private $lookup;

	/** @var PermissionManager */
	private $permissionManager;

	public function __construct(
		UserOptionsLookup $lookup,
		PermissionManager $permissionManager
	) {
		$this->lookup = $lookup;
		$this->permissionManager = $permissionManager;
	}

	/**
	 * Checks whether the user can patrol and has enabled QP on lists.
	 *
	 * @param User $user
	 *
	 * @return bool
	 */
	public function hasQpListsEnabled( User $user ) : bool {
		return $this->permissionManager->userHasRight( $user, 'patrol' ) &&
			$this->lookup->getBoolOption( $user, self::UP_USE_QUICK_PATROL_LISTS );
	}

	public function hasRollbackWithReasonEnabled( User $user ) : bool {
		return $this->lookup->getBoolOption( $user, self::UP_USE_ROLLBACK_WITH_REASON );
	}
}