<?php

namespace FancyModeration;

use MediaWiki\MediaWikiServices;

class FmodServices {
	public static function getPreferences() : Preferences {
		return MediaWikiServices::getInstance()->getService( 'FmodPreferencesService' );
	}
}