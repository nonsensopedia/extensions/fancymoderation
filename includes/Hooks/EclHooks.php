<?php

namespace FancyModeration\Hooks;

use EnhancedChangesList;
use FancyModeration\FmodServices;
use FancyModeration\PatrolLinker;
use FancyModeration\Preferences;
use RCCacheEntry;
use RecentChange;

/**
 * Hooks related to the EnhancedChangesList class
 */
class EclHooks {

	/** @var Preferences */
	private static $preferences;

	/**
	 * Handle RC ungrouped item
	 *
	 * @param EnhancedChangesList $ecl
	 * @param array $data
	 * @param RecentChange|RCCacheEntry $rc
	 *
	 * @return bool
	 */
	public static function onListModifyBlockLineData( EnhancedChangesList $ecl, array &$data, $rc ) : bool {
		if ( !self::getPreferences()->hasQpListsEnabled( $ecl->getUser() ) ) {
			return true;
		}

		if ( $rc->unpatrolled ) {
			$type = $rc->mAttribs['rc_type'];

			// THIS IS PURE EVIL
			// $type can be a string or something, don't change it to ===
			if ( $type == RC_EDIT || $type == RC_NEW ) {
				$data['historyLink'] = substr_replace(
					$data['historyLink'],
					'<span class="patrollink-single">' .
					PatrolLinker::makePatrolSingleLink( $ecl, $rc->mAttribs['rc_this_oldid'] ) . '</span>',
					-7,
					0
				);
			} else {
				// upload log, probably
				$data['fmodPatrol'] = '<span class="fmod-patrol-brackets patrollink-single">' .
					PatrolLinker::makePatrolSingleLink( $ecl, $rc->mAttribs['rc_this_oldid'] ) . '</span>';
			}

			$ecl->getOutput()->addModules( 'ext.fancyModeration.patrol' );
		}

		return true;
	}

	/**
	 * Handle RC group item
	 *
	 * @param EnhancedChangesList $ecl
	 * @param array $data
	 * @param RCCacheEntry[] $block
	 * @param RCCacheEntry $rc
	 *
	 * @return bool
	 */
	public static function onListModifyLineData(
		EnhancedChangesList $ecl, array &$data, $block, RCCacheEntry $rc
	) : bool {
		if ( !self::getPreferences()->hasQpListsEnabled( $ecl->getUser() ) ) {
			return true;
		}

		if ( $rc->unpatrolled ) {
			if ( !empty( $data['currentAndLastLinks'] ) ) {
				$pipe = $ecl->msg( 'pipe-separator' )->text();
				$data['currentAndLastLinks'] = ' ' . $ecl->msg( 'parentheses' )
						->rawParams(
							$rc->curlink . $pipe . $rc->lastlink . $pipe .
							'<span class="patrollink-single">' .
							PatrolLinker::makePatrolSingleLink( $ecl, $rc->mAttribs['rc_this_oldid'] ) . '</span>'
						)->escaped();
				// we don't add the patrol module, it should be added by the parent anyway
			} else {
				// upload log, probably
				$data['fmodPatrol'] = '<span class="fmod-patrol-brackets patrollink-single">' .
					PatrolLinker::makePatrolSingleLink( $ecl, $rc->mAttribs['rc_this_oldid'] ) . '</span>';
				$ecl->getOutput()->addModules( 'ext.fancyModeration.patrol' );
			}
		}

		return true;
	}

	/**
	 * Handle RC group header
	 *
	 * @param EnhancedChangesList $ecl
	 * @param array $links
	 * @param RCCacheEntry[] $block
	 *
	 * @return bool
	 */
	public static function onGetLogText( EnhancedChangesList $ecl, array &$links, $block ) : bool {
		if ( !self::getPreferences()->hasQpListsEnabled( $ecl->getUser() ) ) {
			return true;
		}

		if ( sizeof( $links ) === 0 ) {
			// logs grouped by type probably, let's not get involved
			return true;
		}

		$toPatrol = [];
		$toPatrolMax = 0;
		foreach ( $block as $item ) {
			if ( $item->unpatrolled ) {
				$pageId = $item->mAttribs['rc_cur_id'];

				if ( isset( $toPatrol[$pageId] ) ) {
					$toPatrol[$pageId]++;
				} else {
					$toPatrol[$pageId] = 1;
				}

				if ( $item->mAttribs['rc_this_oldid'] > $toPatrolMax ) {
					$toPatrolMax = $item->mAttribs['rc_this_oldid'];
				}
			}
		}

		if ( sizeof( $toPatrol ) !== 1 || $toPatrolMax === 0 ) {
			return true;
		}

		$links['fmodPatrol'] = '<span class="patrollink-range">' .
			PatrolLinker::makePatrolRangeLink( $ecl, $toPatrolMax ) . '</span>';
		$ecl->getOutput()->addModules( 'ext.fancyModeration.patrol' );

		return true;
	}

	/**
	 * Because I'm too lazy to convert this to a non-static class.
	 * Lazy, get it? Haha, haha...
	 *
	 * ha.
	 *
	 * @return Preferences
	 */
	private static function getPreferences() : Preferences {
		if ( !isset( self::$preferences ) ) {
			self::$preferences = FmodServices::getPreferences();
		}
		return self::$preferences;
	}
}