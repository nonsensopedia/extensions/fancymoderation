<?php

namespace FancyModeration\Hooks;

use FancyModeration\Preferences;
use MediaWiki\MediaWikiServices;
use MediaWiki\Preferences\Hook\GetPreferencesHook;
use User;

class GetPreferences implements GetPreferencesHook {

	/**
	 * @param User $user
	 * @param array $preferences
	 *
	 * @return void
	 */
	public function onGetPreferences( $user, &$preferences ) {
		$pm = MediaWikiServices::getInstance()->getPermissionManager();

		if ( $pm->userHasRight( $user, 'rollback' ) ) {
			$preferences[Preferences::UP_USE_ROLLBACK_WITH_REASON] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-rollback-with-reason',
				'section' => 'moderation/rollback',
			];
			// TODO: Uncomment this later, when implemented
			/*$preferences[Preferences::UP_USE_ROLLBACK_CONFIRMATION] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-rollback-confirmation',
				'section' => 'moderation/rollback',
			];*/
		}

		if ( $pm->userHasRight( $user, 'patrol' ) ) {
			$preferences[Preferences::UP_USE_QUICK_PATROL_LISTS] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-quickpatrol-lists',
				'section' => 'moderation/patrol',
			];
			$preferences[Preferences::UP_USE_QUICK_PATROL_NEWPAGES] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-quickpatrol-newpages',
				'section' => 'moderation/patrol',
			];
			$preferences[Preferences::UP_USE_QUICK_PATROL_DIFFS] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-quickpatrol-diffs',
				'section' => 'moderation/patrol',
			];
			$preferences[Preferences::UP_USE_QUICK_PATROL_FOOTER] = [
				'type' => 'toggle',
				'label-message' => 'fmod-use-quickpatrol-footer',
				'section' => 'moderation/patrol',
			];
		}
	}
}