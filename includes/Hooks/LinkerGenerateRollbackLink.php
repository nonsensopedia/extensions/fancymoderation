<?php

namespace FancyModeration\Hooks;

use FancyModeration\Preferences;
use Html;
use IContextSource;
use MediaWiki\Linker\Hook\LinkerGenerateRollbackLinkHook;
use MediaWiki\Storage\RevisionRecord;
use OutputPage;

class LinkerGenerateRollbackLink implements LinkerGenerateRollbackLinkHook {

	/** @var Preferences */
	private $preferences;

	public function __construct(
		Preferences $preferences
	) {
		$this->preferences = $preferences;
	}

	/**
	 * @param RevisionRecord $revRecord
	 * @param IContextSource $context
	 * @param array $options
	 * @param string $inner
	 *
	 * @return bool
	 */
	public function onLinkerGenerateRollbackLink( $revRecord, $context, $options, &$inner ) {
		if ( $this->preferences->hasRollbackWithReasonEnabled( $context->getUser() ) ) {
			$targetUser = $revRecord->getUser(
				RevisionRecord::FOR_THIS_USER,
				$context->getUser()
			);
			if ( $targetUser ) {
				$link = Html::element(
					'a',
					[
						'title' => $context->msg( 'fmod-reason-tooltip' )->text(),
						'class' => 'mw-rollback-reason-link',
						'data-page-title' => $revRecord->getPageAsLinkTarget()->getFullText(),
						'data-user' => $targetUser->getName(),
					],
					$context->msg( 'fmod-reason' )->text()
				);
				$inner .= $context->msg( 'pipe-separator' )->text() . Html::rawElement(
						'span',
						[ 'class' => 'mw-rollback-reason-link' ],
						$link
					);
			}
		}

		$this->addModules( $context->getOutput() );
		return true;
	}

	private function addModules( OutputPage $outputPage ) {
		if ( in_array( 'ext.fancyModeration.rollback', $outputPage->getModules() ) ) {
			return;
		}
		$outputPage->addModuleStyles( 'ext.fancyModeration.rollback.styles' );
		$outputPage->addModules( 'ext.fancyModeration.rollback' );
		$outputPage->addJsConfigVars( [
			'wgFmodRollbackReasons' => $this->getRollbackReasons( $outputPage ),
			'wgFmodRollbackSummary' => $outputPage->msg( 'fmod-rollback-reason-message' )
					->inContentLanguage()->plain()
		] );
	}

	private function getRollbackReasons( IContextSource $context ) {
		$reasons = [];
		$rawList = $context->msg( 'fmod-rollback-reason-list' )
			->inContentLanguage()->plain() ?: '';

		foreach ( explode( "\n", $rawList ) as $item ) {
			$item = trim( $item );
			if ( $item === '' ) continue;
			$reasons[] = preg_replace( '/^\*\s*/', '', $item );
		}

		return $reasons;
	}
}