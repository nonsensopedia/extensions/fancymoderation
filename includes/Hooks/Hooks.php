<?php

namespace FancyModeration\Hooks;

use LogEntry;
use RecentChange;
use User;
use WikiPage;

/**
 * General hooks.
 */
class Hooks {

	/**
	 * Removes the patrolmark for uploaded files when they are deleted.
	 * This is a bit of a hack, as it marks the upload as "autopatrolled", which is of course not true, but the end
	 * result is satisfactory.
	 *
	 * @param WikiPage $article
	 * @param User $user
	 * @param $reason
	 * @param $id
	 * @param $content
	 * @param LogEntry $logEntry
	 * @param $archivedRevisionCount
	 */
	public static function onArticleDeleteComplete(
		WikiPage &$article, User &$user, $reason, $id, $content, LogEntry $logEntry, $archivedRevisionCount
	) {
		if ( $article->getTitle()->getNamespace() !== NS_FILE ) {
			return;
		}

		$dbw = wfGetDB( DB_PRIMARY );
		$dbw->update( 'recentchanges',
			[ 'rc_patrolled' => RecentChange::PRC_AUTOPATROLLED ],
			[
				'rc_patrolled' => RecentChange::PRC_UNPATROLLED,
				'rc_log_action' => 'upload',
				'rc_cur_id' => $article->getId()
			],
			__METHOD__ );
	}
}