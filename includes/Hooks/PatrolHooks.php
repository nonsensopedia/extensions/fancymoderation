<?php

namespace FancyModeration\Hooks;

use Article;
use DifferenceEngine;
use FancyModeration\PatrolLinker;
use FancyModeration\Preferences;
use Html;
use MediaWiki\MediaWikiServices;
use SpecialNewpages;

class PatrolHooks {

	/**
	 * Add patrol links in diff view.
	 *
	 * @param DifferenceEngine $de
	 * @param string $link
	 * @param $rcId
	 */
	public static function onDifferenceEngineMarkPatrolledLink( DifferenceEngine $de, string &$link, $rcId ) : void {
		if ( !$link ) {
			return; // no patrol link here
		}

		// TODO: replace with DI
		$services = MediaWikiServices::getInstance();
		$optionsLookup = $services->getUserOptionsLookup();

		if ( !$optionsLookup->getBoolOption( $de->getUser(), Preferences::UP_USE_QUICK_PATROL_DIFFS  ) ) {
			return;
		}

		$de->loadRevisionData();
		$oldId = $de->getOldid();
		$newId = $de->getNewid();

		// if $oldId is 0 we leave the existing link as it is
		if ( $oldId !== 0 ) {
			$old = $de->getOldRevision();
			if ( $old ) {
				$store = $services->getRevisionStore();

				// if $old is not the first revision, we shouldn't patrol it
				if ( $store->getPreviousRevision( $old ) ) {
					$rev = $store->getNextRevision( $old );

					if ( $rev ) {
						$oldId = $rev->getId();
					}
				}
			}

			$link = Html::rawElement( 'span',
				[ 'class' => 'patrollink-range' ],
				'[' . PatrolLinker::makePatrolRangeLink(
					$de,
					$newId,
					$oldId
				) . ']'
			);
		}

		if ( $oldId === 0 || $oldId ) {
			$newRev = $de->getNewRevision();

			if ( $newRev ) {
				$link .=
					Html::rawElement( 'br' ) .
					Html::rawElement( 'span',
						[ 'class' => 'patrollink-page' ],
						'[' . PatrolLinker::makePatrolPageLink(
							$de,
							$newRev->getPageId()
						) . ']'
					);
			}
		}

		$de->getOutput()->addModules( 'ext.fancyModeration.patrol' );
	}

	/**
	 * Modify the patrol link in article's footer.
	 *
	 * @param Article $article
	 * @param $patrolFooterShown
	 */
	public static function onArticleViewFooter( Article $article, $patrolFooterShown ) {
		if ( !$patrolFooterShown ) {
			return;
		}

		// TODO: replace with DI
		$services = MediaWikiServices::getInstance();
		$optionsLookup = $services->getUserOptionsLookup();
		if ( !$optionsLookup->getBoolOption(
			$article->getContext()->getUser(), Preferences::UP_USE_QUICK_PATROL_FOOTER
		) ) {
			return;
		}

		$footerRegex = '#\<div class\=["\']patrollink["\'] data\-mw\=["\']interface["\']\>.*?\</div\>#';
		$out = $article->getContext()->getOutput();
		$html = $out->getHTML();

		if ( preg_match( $footerRegex,
			$html,
			$m ) ) {
			$v = preg_replace( '/href=".*?"/',
				'data-pageid="' . $article->getPage()->getId() . '"',
				$m[0] );
			$v = str_replace( 'patrollink',
				'patrollink patrollink-page',
				$v );
			$html = str_replace( $m[0],
				$v,
				$html );
			$out->clearHTML();
			$out->addHTML( $html );
			$out->addModules( 'ext.fancyModeration.patrol' );
		}
	}

	/**
	 * Add patrol links on Special:NewPages
	 *
	 * @param SpecialNewpages $page
	 * @param string $line
	 * @param $row
	 * @param array $classes
	 * @param array $attribs
	 */
	public static function onNewPagesLineEnding(
		SpecialNewpages $page, string &$line, $row, array &$classes, array &$attribs
	) {
		if ( !in_array( 'not-patrolled',
			$classes ) ) {
			return;
		}

		// TODO: replace with DI
		$services = MediaWikiServices::getInstance();
		$optionsLookup = $services->getUserOptionsLookup();
		$user = $page->getUser();

		if ( !$optionsLookup->getBoolOption( $user, Preferences::UP_USE_QUICK_PATROL_NEWPAGES ) ) {
			return;
		}

		$histRegex = '#\<a[^\<]*?action=history[^\<]*?\</a\>([^\<]*\<a[^\<]*?\</a\>)*#';
		if ( !preg_match( $histRegex,
			$line,
			$matches ) ) {
			return;
		}

		$histLink = $matches[0];
		$patrolLink = PatrolLinker::makePatrolPageLink( $page->getContext(),
			$row->rc_cur_id );
		$newHistLink = $histLink . $page->msg( 'pipe-separator' )
				->escaped() . '<span class="patrollink-page">' . $patrolLink . '</span>';

		$line = str_replace( $histLink,
			$newHistLink,
			$line );
		$page->getOutput()->addModules( 'ext.fancyModeration.patrol' );
	}
}
