<?php

namespace FancyModeration\Api;

use ApiBase;
use ApiMain;
use ApiPatrol;
use ApiQueryBase;
use ApiUsageException;
use ChangeTags;
use MediaWiki\MediaWikiServices;
use MediaWiki\Revision\RevisionStore;
use Title;
use Wikimedia\ParamValidator\ParamValidator;

/**
 * Api endpoint handling all the patrol-related needs.
 */
class ApiFmodPatrol extends ApiPatrol {

	/**
	 * Explicit constructor to avoid DI surprises when core changes the ApiPatrol class.
	 *
	 * @param ApiMain $main
	 * @param string $action
	 * @param RevisionStore $revisionStore
	 */
	public function __construct(
		ApiMain $main,
		$action,
		RevisionStore $revisionStore
	) {
		parent::__construct( $main, $action, $revisionStore );
	}

	/**
	 * Main entry point
	 *
	 * @throws ApiUsageException
	 */
	public function execute() {
		// TODO: replace with DI
		$wpFactory = MediaWikiServices::getInstance()->getWikiPageFactory();

		$params = $this->extractRequestParams();
		$this->requireOnlyOneParameter( $params,
			'revid',
			'pageid',
			'maxrevid' );

		// Find revisions to patrol
		$store = MediaWikiServices::getInstance()->getRevisionStore();
		$minrev = null;
		if ( isset( $params['revid'] ) ) {
			$maxrev = $store->getRevisionById( $params['revid'] );
			if ( !$maxrev ) {
				$this->dieWithError( [ 'apierror-nosuchrevid',
					$params['revid'] ] );
			}

			$minrev = $maxrev;
		} elseif ( isset( $params['pageid'] ) ) {
			$page = $wpFactory->newFromID( $params['pageid'] );
			if ( !$page ) {
				$this->dieWithError( [ 'apierror-nosuchpageid',
					$params['pageid'] ] );
			}

			$maxrev = $store->getRevisionByPageId( $page->getId() );
		} else {
			$maxrev = $store->getRevisionById( $params['maxrevid'] );
			if ( !$maxrev ) {
				$this->dieWithError( [ 'apierror-nosuchrevid',
					$params['maxrevid'] ] );
			}

			if ( isset( $params['minrevid'] ) ) {
				if ( $params['minrevid'] > $params['maxrevid'] ) {
					$this->dieWithError( [ 'apierror-integeroutofrange-belowminimum',
						'maxrevid',
						$params['minrevid'],
						$params['maxrevid'] ] );
				}

				$minrev = $store->getRevisionById( $params['minrevid'] );
				if ( !$minrev ) {
					$this->dieWithError( [ 'apierror-nosuchrevid',
						$params['minrevid'] ] );
				}

				if ( $minrev->getPageId() !== $maxrev->getPageId() ) {
					$this->dieWithError( [ 'apierror-revsfromdifferentpages',
						$params['minrevid'],
						$params['maxrevid'] ] );
				}
			}
		}

		$user = $this->getUser();
		$tags = $params['tags'];

		// Check if user can add tags
		if ( $tags !== null ) {
			$ableToTag = ChangeTags::canAddTagsAccompanyingChange( $tags,
				$user );
			if ( !$ableToTag->isOK() ) {
				$this->dieStatus( $ableToTag );
			}
		}

		// Do the patrol dance
		$rev = $maxrev;
		$patrolledEdits = 0;
		while ( $rev ) {
			if ( $minrev && $rev->getId() < $minrev->getId() ) {
				break;  // we reached the minimum revision in the range
			}

			$rc = $store->getRecentChange( $rev );
			if ( !$rc ) {
				break;  // this is the oldest revision of this page in RC, the end
			}

			if ( !$rc->getAttribute( 'rc_patrolled' ) ) {
				$retval = $rc->doMarkPatrolled( $user,
					false,
					$tags );

				if ( $retval ) {
					$this->dieStatus( $this->errorArrayToStatus( $retval,
						$user ) );
				} else {
					$patrolledEdits++;
				}
			}

			$rev = $store->getPreviousRevision( $rev );
		}

		// Return stuff
		$title = Title::newFromID( $maxrev->getPageId() );
		$result = [ 'patrollededits' => $patrolledEdits ];
		ApiQueryBase::addTitleInfo( $result,
			$title );
		$this->getResult()->addValue( null,
			$this->getModuleName(),
			$result );
	}

	/**
	 * @return array
	 */
	public function getAllowedParams() {
		return [
			'revid' => [
				ParamValidator::PARAM_TYPE => 'integer',
				ApiBase::PARAM_HELP_MSG => 'apihelp-patrol-param-revid'
			],
			'pageid' => [ ParamValidator::PARAM_TYPE => 'integer' ],
			'maxrevid' => [ ParamValidator::PARAM_TYPE => 'integer' ],
			'minrevid' => [ ParamValidator::PARAM_TYPE => 'integer' ],
			'tags' => [
				ParamValidator::PARAM_TYPE => 'tags',
				ParamValidator::PARAM_ISMULTI => true,
				ApiBase::PARAM_HELP_MSG => 'apihelp-patrol-param-tags'
			],
		];
	}

	/**
	 * Returns example requests.
	 *
	 * @return array
	 */
	protected function getExamplesMessages() {
		return [];  // TODO: implement
	}
}